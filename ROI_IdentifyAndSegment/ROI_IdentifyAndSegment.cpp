// ROI_IdentifyAndSegment.cpp : Defines the entry point for the console application.
//

#include "stdafx.h"
#include <opencv2\core.hpp>
#include <opencv2\highgui.hpp>
#include <opencv2\imgcodecs.hpp>
#include <opencv2\imgproc\imgproc.hpp>

#include <iostream>
#include <sstream>
#include <vector>
#include <fstream>

cv::Mat fFill(const cv::Mat& src) 
{
	// Ref : https://www.learnopencv.com/filling-holes-in-an-image-using-opencv-python-c/
	// This function Performs a floodfill operation on the src image which will fill in all the holes 
	// and returns dst the resulting binary filled image 
	// Note : We assume that black is background and that the image is binary
	// src : input binary image
	// dst : the filled image
		
	// filling the inverted image
	cv::Mat src_ffill = (255 - src);
	
	cv::floodFill(src_ffill, cv::Point(0, 0), cv::Scalar(0));
	
	// Combining the two images to get the foreground mask
	cv::Mat dst = (src | src_ffill);

	return dst;
}


int main(int argc, char* argv[]) // Enter the path to the whole slide image in the commandline
{
	if (argc < 2)
	{
		std::cout << "ERROR: Not enough input commandline args" << std::endl;
		return -2;
	}

	// Reading the histology slide image
	//cv::Mat img_color = cv::imread(argv[1], cv::IMREAD_COLOR);
	cv::Mat img_gray = cv::imread(argv[1], cv::ImreadModes::IMREAD_GRAYSCALE);

	if (img_gray.empty())
	{
		std::cout << "ERROR : Image not loaded" << std::endl;
		return -1;
	}

	//float down_factor = 10; // downsampling factor
	/*
	// Downsampling the image
	cv::Mat img_color_down; // initializing the downsampled color image
	cv::resize(img_color, img_color_down, cv::Size(), 1 / down_factor, 1 / down_factor);
	*/

	// converting the downsampled image to grayscale
	cv::Mat img_gray_down;  // initializing the downsampled grayscale image
	//cv::resize(img_gray_down, img_gray, cv::Size(), 1 / down_factor, 1 / down_factor);
	//cv::cvtColor(img_color_down, img_gray_down, cv::ColorConversionCodes::COLOR_BGR2GRAY);
	img_gray_down = img_gray;

	// Thresholding and getting a binary image 
	int im_thresh = 25;
	cv::Mat img_bin_down = ((255 - img_gray_down) > im_thresh); // img_down - > the downsampled binary image

	// Doing an opening operation on the image to remove the small smudges in the background
	int struct_elem_sz = std::max(img_bin_down.cols,img_bin_down.rows)/100;
	cv::Mat struct_elem = cv::getStructuringElement(cv::MorphShapes::MORPH_ELLIPSE, cv::Size(struct_elem_sz, struct_elem_sz));
	cv::morphologyEx(img_bin_down,img_bin_down,cv::MorphTypes::MORPH_OPEN, struct_elem);
	cv::morphologyEx(img_bin_down, img_bin_down, cv::MorphTypes::MORPH_DILATE, struct_elem,cv::Point(-1,-1),1);


	// Doing a floodflill operation on the image to get filled in objects with no holes
	cv::Mat img_bin_down_open_fill = fFill(img_bin_down);
	
	// Clearing the original image and the binary image to reduce RAM required
	//img.deallocate();
	//img_bin.deallocate();
	
	// Writing a two step procedure to remove all small blobs and take only the epithelium masks
	cv::Mat imgLabels;
	cv::Mat imgStats;
	cv::Mat imgCentroids;
	int nLabels = cv::connectedComponentsWithStats(img_bin_down_open_fill, imgLabels, imgStats, imgCentroids, 8);

	// Sorting the indices(Labels) of the connected objects according to the area size
	cv::Mat area_stats = imgStats.col(cv::CC_STAT_AREA).rowRange(1, nLabels); // all the area stats except for the background (the 0th row inde)
	cv::Mat sortedLabels_area;
	cv::sortIdx(area_stats, sortedLabels_area, cv::SORT_EVERY_COLUMN + cv::SORT_DESCENDING);
	
	// Iterating through the largest connected components
	// Note : if the connected objects area is a given percent of the maximum area then we will stop the iteration
	float area_thresh = 0.20; // 20 percent of the maximum area
	int max_area = area_stats.at<int>(sortedLabels_area.at<int>(0,0), 0);
	int obj_area = max_area;
	
	
	// Stringstream and string objects to create filenames for images in the loop below
	std::string imgName;
	std::stringstream fileName;
	



	for (int idx = 0; (idx < nLabels - 1) && (obj_area > (area_thresh * max_area)); idx++)
	{
		cv::Mat object_roi;
		cv::Mat connected_obj = (imgLabels == sortedLabels_area.at<int>(idx, 0) + 1); 
		
		//img_color_down.copyTo( object_roi, connected_obj);

		// Below we also check wether the lookahead exceeds the number if labels available so that out of bounds error doesnt happen
		obj_area = (idx < area_stats.rows - 1 )? area_stats.at<int>(sortedLabels_area.at<int>(idx + 1, 0), 0) : 0; // lookahead to check area of the next connected obj
		
		//cv::Mat stats_obj = imgStats.row(sortedLabels_area.at<int>(idx, 0) + 1).colRange(0,cv::CC_STAT_AREA); // all the img stats corresponding to the current roi obj
				
		/*
		// Creating a rectangle w.r.t to the bounding box for the roi object
		cv::Rect myROI(stats_obj.at<int>(0,cv::CC_STAT_LEFT) , 
			stats_obj.at<int>(0, cv::CC_STAT_TOP), 
			stats_obj.at<int>(0, cv::CC_STAT_WIDTH), 
			stats_obj.at<int>(0, cv::CC_STAT_HEIGHT));
		
		// the roi object
		cv::Mat roi_obj = object_roi(myROI);
		*/
		
		// Saving the ROI image
		fileName << "img_label_" << idx << ".jpg";
		imgName = fileName.str();

		cv::imwrite(imgName, connected_obj);
		fileName.str("");
	    
	}



	/*
	std::cout << "resized !!!!!!!!!!!";
	short temp;
	std::cin >> temp;
	*/

	return 0;
}

